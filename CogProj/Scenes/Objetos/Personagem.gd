tool
extends AnimatedSprite
signal personagem_clicado

export(String) var texto_balao = "" setget set_texto_balao, get_texto_balao

func _ready():
	if (!Engine.editor_hint):
		$Balao.hide()
		$Zona.connect("mouse_entered", self, "_mouse_dentro")
		$Zona.connect("mouse_exited", self, "_mouse_fora")
		$Zona.connect("input_event", self, "_alterna_texto")

func set_texto_balao(novo_texto):
	texto_balao = novo_texto
	if (has_node("Balao")):
		#TODO nodo texto
		pass
		
func get_texto_balao():
	return texto_balao
	
func _alterna_texto(viewport, event, shape_index):
	if event is InputEventMouseButton and event.pressed and get_parent().intro_done:
		emit_signal("personagem_clicado")
		if $Balao.visible:
			$Balao.hide()
		else:
			$Balao.show()
	
func _mouse_dentro():	
	if get_parent().intro_done:
		$tween.interpolate_property(self, 'scale', scale, Vector2(0.55,0.55), 0.25, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		$tween.start()
	
	
func _mouse_fora():
	if get_parent().intro_done:
		$tween.interpolate_property(self, 'scale', scale, Vector2(0.5, 0.5), 0.1, Tween.TRANS_QUAD, Tween.EASE_OUT)
		if $Balao.visible:
			$Balao.hide()
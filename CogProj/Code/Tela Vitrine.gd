extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():	
	$BIA/BIAButton.connect("mouse_entered", self, "_mouse_dentro",["BIA"])
	$BIA/BIAButton.connect("mouse_exited", self, "_mouse_fora",["BIA"])
	$BIA/BIAButton.connect("pressed",self,"_clicado",["BIA"])
	$IRU/IRUButton.connect("mouse_entered", self, "_mouse_dentro",["IRU"])
	$IRU/IRUButton.connect("mouse_exited", self, "_mouse_fora",["IRU"])
	$IRU/IRUButton.connect("pressed",self,"_clicado",["IRU"])
	
	$Voltar/VoltarButton.connect("mouse_entered", self, "_mouse_dentro",["Voltar"])
	$Voltar/VoltarButton.connect("mouse_exited", self, "_mouse_fora",["Voltar"])
	$Voltar/VoltarButton.connect("pressed",self,"_clicado",["Voltar"])

func _mouse_dentro(nome):
	if nome == "BIA":
		$tween.interpolate_property($BIA, 'scale', $BIA.scale, Vector2(0.85,0.85), 0.1, Tween.TRANS_QUAD, Tween.EASE_IN)
		$tween.start()
	elif nome == "IRU":
		$tween.interpolate_property($IRU, 'scale', $IRU.scale, Vector2(0.85,0.85), 0.1, Tween.TRANS_QUAD, Tween.EASE_IN)
		$tween.start()
	elif nome == "Voltar":
		$tween.interpolate_property($Voltar, 'scale', $Voltar.scale, Vector2(1.05,1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$tween.start()
	
func _mouse_fora(nome):
	if nome == "BIA":
		$tween.interpolate_property($BIA, 'scale', $BIA.scale, Vector2(0.8,0.8), 0.1, Tween.TRANS_QUAD, Tween.EASE_OUT)
		$tween.start()
	elif nome == "IRU":
		$tween.interpolate_property($IRU, 'scale', $IRU.scale, Vector2(0.8,0.8), 0.1, Tween.TRANS_QUAD, Tween.EASE_OUT)
		$tween.start()
	elif nome == "Voltar":
		$tween.interpolate_property($Voltar, 'scale', $Voltar.scale, Vector2(1,1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$tween.start()
		
func _clicado(nome):
	if nome == "BIA":
		get_tree().change_scene("res://Scenes/BIA.tscn")
	elif nome == "IRU":
		get_tree().change_scene("res://Scenes/IRU.tscn")
	elif nome == "Voltar":
		get_tree().change_scene("res://Scenes/Tela Inicial.tscn")
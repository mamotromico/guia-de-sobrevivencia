extends Node2D

var intro_finished = false;
var intro_done = false;

func _ready():
	$Titulo/ATitulo.connect("animation_finished",self,"_set_intro_finished", [], CONNECT_ONESHOT)
	$Entrar.hide()
	$Gabriel.connect("personagem_clicado", self, "_mostra_porta")
	#$Outro.connect("personagem_clicado", self, "_mostra_porta")
	$Raissa.connect("personagem_clicado", self, "_mostra_porta")
	#$Vitoria.connect("personagem_clicado", self, "_mostra_porta")

func _mostra_porta():
	if intro_done:
		$Entrar.show();
		$Entrar.connect("pressed", self, "_troca_cena")
		$Gabriel.disconnect("personagem_clicado", self, "_mostra_porta")
		#$Outro.disconnect("personagem_clicado", self, "_mostra_porta")
		$Raissa.disconnect("personagem_clicado", self, "_mostra_porta")
		#$Vitoria.disconnect("personagem_clicado", self, "_mostra_porta")

func _troca_cena():
	get_tree().change_scene("res://Scenes/Tela Vitrine.tscn")

func _set_intro_finished(name):
	intro_finished = true;
	$Titulo/ATitulo.connect("animation_finished", self, "_set_intro_done", [], CONNECT_ONESHOT)

func _set_intro_done(name):
	intro_done = true;

func _input(event):
	if event is InputEventMouseButton and event.pressed and intro_finished == true and $Titulo.modulate.a == 1:
		$Titulo/ATitulo.play("Fade_out");
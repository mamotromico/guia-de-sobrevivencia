extends Node2D

var textJoia = """O Aluno que tiver sua confirmação no Programa de Iniciação Acadêmica (B.I.A.), deve compararecer ao local e data disponibilizados pela PRAE para participar da Jornada de Iniciação Acadêmica, onde ocorre uma palestra sobre a B.I.A. e são divulgados os projetos para quais os alunos foram alocados. Caso tenha aula no mesmo horário do encontro, o aluno tem direito a pegar uma declaração para justificar sua falta."""
var textFragil = """O Aluno que se inscrever na Bolsa de Iniciação Acadêmica deve apresentar fragilidade econômica compravada de até um salário mínimo que inviabilize sua permanência no curso, mediante da entrega da documentação requerida pelo edital da PRAE."""
var text2anos = """O Programa Bolsa de Iniciação Acadêmica (B.I.A.) não será disponibilizado para alunos que já tenha participado ou sido beneficiado com o programa por 2 anos, ou seja, não há terceiro ano de B.I.A., mesmo que tenha sido beneficiado por alguns meses da bolsa e tenha desistido durante o ano letivo."""
var textDoc = """As principais cópias de documento necessárias para a participação do Programa de Iniciação Acadêmica (B.I.A.) são:
				Identidade(R.G.), CPF, Comprovante de Residência, Comprovante de Matrícula na UFC, Comprovação de ingresso na UFC por cotas e Dados Bancários de conta corrente do estudante (xerox do cartão ou da abertura da conta, se tiver). Os demais documentos solicitados serão disponibilizados pelo próprio edital da B.I.A. pelo site da PRAE"""
var textHoras = """Dispor de 12 (doze) horas semanais, de segunda-feira a sexta-feira, para cumprimento das atividades da Bolsa em que estiver frequetando, em três dias de 4 (quatro) horas ou quatro dias de 3 (três) horas.
				Exemplo: segunda feira terão disponiveis 4 horas para o trabalho da bolsa, assim como na quarta ou sexta, depedendo de sua escolha."""

func _ready():
	$IconeJoia/Area.connect("mouse_entered", self, "_mouse_dentro", ["joia"])
	$IconeJoia/Area.connect("mouse_exited", self, "_mouse_fora", ["joia"])
	$IconeJoia/Area.connect("input_event", self, "_seleciona_topico", ["joia"])
	
	$Icone2Anos/Area.connect("mouse_entered", self, "_mouse_dentro", ["anos"])
	$Icone2Anos/Area.connect("mouse_exited", self, "_mouse_fora", ["anos"])
	$Icone2Anos/Area.connect("input_event", self, "_seleciona_topico", ["anos"])
	
	$IconeDocumentos/Area.connect("mouse_entered", self, "_mouse_dentro", ["doc"])
	$IconeDocumentos/Area.connect("mouse_exited", self, "_mouse_fora", ["doc"])
	$IconeDocumentos/Area.connect("input_event", self, "_seleciona_topico", ["doc"])
	
	$IconeFragilidadeEconomica/Area.connect("mouse_entered", self, "_mouse_dentro", ["fragil"])
	$IconeFragilidadeEconomica/Area.connect("mouse_exited", self, "_mouse_fora", ["fragil"])
	$IconeFragilidadeEconomica/Area.connect("input_event", self, "_seleciona_topico", ["fragil"])
	
	$IconeHorasSemanais/Area.connect("mouse_entered", self, "_mouse_dentro", ["horas"])
	$IconeHorasSemanais/Area.connect("mouse_exited", self, "_mouse_fora", ["horas"])
	$IconeHorasSemanais/Area.connect("input_event", self, "_seleciona_topico", ["horas"])
	
	$Voltar/VoltarButton.connect("mouse_entered", self, "_mouse_dentro",["Voltar"])
	$Voltar/VoltarButton.connect("mouse_exited", self, "_mouse_fora",["Voltar"])
	$Voltar/VoltarButton.connect("pressed",self,"_clicado",["Voltar"])

func _seleciona_topico(viewport, event, shape_index, nome):
	if event is InputEventMouseButton and event.pressed:
		$AJoia.hide()
		$AAnos.hide()
		$ADoc.hide()
		$AHoras.hide()
		$AFragil.hide()
		if nome == "joia":
			$Titulo.text = "JOIA"
			$Texto.text = textJoia
			$AJoia.show()
			$AJoia/player.play("joia")
		elif nome == "anos":
			$Titulo.text = "TEMPO LIMITE"
			$Texto.text = text2anos
		elif nome == "horas":
			$Titulo.text = "HORAS DISPONIBILIZADAS"
			$Texto.text = textHoras			
		elif nome == "fragil":
			$Titulo.text = "FRAGILIDADE ECONÔMICA"
			$Texto.text = textFragil
			$AFragil.show()
			$AFragil/player.play("cofre")
		elif nome == "doc":
			$Titulo.text = "DOCUMENTAÇÃO"
			$Texto.text = textDoc
			$ADoc.show()
			$ADoc/player.play("doc")


func _mouse_dentro(nome):
	if nome == "Voltar":
		$tween.interpolate_property($Voltar, 'scale', $Voltar.scale, Vector2(1.05,1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$tween.start()
	elif nome == "joia":
		$tween.interpolate_property($IconeJoia, 'scale', $IconeJoia.scale, Vector2(1.05, 1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "anos":
		$tween.interpolate_property($Icone2Anos, 'scale', $Icone2Anos.scale, Vector2(1.05, 1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "horas":
		$tween.interpolate_property($IconeHorasSemanais, 'scale', $IconeHorasSemanais.scale, Vector2(1.05, 1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "fragil":
		$tween.interpolate_property($IconeFragilidadeEconomica, 'scale', $IconeFragilidadeEconomica.scale, Vector2(1.05, 1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "doc":
		$tween.interpolate_property($IconeDocumentos, 'scale', $IconeDocumentos.scale, Vector2(1.05, 1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	
func _mouse_fora(nome):
	if nome == "Voltar":
		$tween.interpolate_property($Voltar, 'scale', $Voltar.scale, Vector2(1,1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$tween.start()
	elif nome == "joia":
		$tween.interpolate_property($IconeJoia, 'scale', $IconeJoia.scale, Vector2(1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "anos":
		$tween.interpolate_property($Icone2Anos, 'scale', $Icone2Anos.scale, Vector2(1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "horas":
		$tween.interpolate_property($IconeHorasSemanais, 'scale', $IconeHorasSemanais.scale, Vector2(1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "fragil":
		$tween.interpolate_property($IconeFragilidadeEconomica, 'scale', $IconeFragilidadeEconomica.scale, Vector2(1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "doc":
		$tween.interpolate_property($IconeDocumentos, 'scale', $IconeDocumentos.scale, Vector2(1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
		
func _clicado(nome):
	if nome == "Voltar":
		get_tree().change_scene("res://Scenes/Tela Vitrine.tscn")
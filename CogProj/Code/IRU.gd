extends Node2D

var textTcc = """O aluno não deverá estar matriculado apenas em disciplinas de estágio curricular ou Trabalho de Conclusão de Curso (TCC). Caso seja bolsista do Programa B.I.A., não há problema dentro deste critério."""
var textFragil = """O aluno que possuir renda per capita familiar de até meio salário mínimo ou renda bruta familiar de até três salários. Caso seja bolsista do Programa Bolsa de Iniciação Acadêmica, basta assinar um termo disbonibilazado pela PRAE e levar a mesma que irá comprovar sua situação financeira e irá garantir sua inseção no Restaurante Universitário."""
var textCredits = """Comprovar matrícula em no mínimo 12 créditos em um curso de graduação presencial, exceto em situações em que a própria grade curricular não permita a aquisição de créditos. Caso seja bolsista do Programa B.I.A., isso não é necessário."""

func _ready():
	$iconeFragilidadeEconomicaRU/Area.connect("mouse_entered", self, "_mouse_dentro", ["fragil"])
	$iconeFragilidadeEconomicaRU/Area.connect("mouse_exited", self, "_mouse_fora", ["fragil"])
	$iconeFragilidadeEconomicaRU/Area.connect("input_event", self, "_seleciona_topico", ["fragil"])
	
	$IconeCreditosDasCadeiras/Area.connect("mouse_entered", self, "_mouse_dentro", ["creditos"])
	$IconeCreditosDasCadeiras/Area.connect("mouse_exited", self, "_mouse_fora", ["creditos"])
	$IconeCreditosDasCadeiras/Area.connect("input_event", self, "_seleciona_topico", ["creditos"])
	
	$IconeNaoPodeTCCnemTrabalho/Area.connect("mouse_entered", self, "_mouse_dentro", ["tcc"])
	$IconeNaoPodeTCCnemTrabalho/Area.connect("mouse_exited", self, "_mouse_fora", ["tcc"])
	$IconeNaoPodeTCCnemTrabalho/Area.connect("input_event", self, "_seleciona_topico", ["tcc"])
	
	$Voltar/VoltarButton.connect("mouse_entered", self, "_mouse_dentro",["Voltar"])
	$Voltar/VoltarButton.connect("mouse_exited", self, "_mouse_fora",["Voltar"])
	$Voltar/VoltarButton.connect("pressed",self,"_clicado",["Voltar"])

func _seleciona_topico(viewport, event, shape_index, nome):
	if event is InputEventMouseButton and event.pressed:
		if nome == "fragil":
			$Titulo.text = "FRAGILIDADE ECONÔMICA"
			$Texto.text = textFragil
		elif nome == "creditos":
			$Titulo.text = "CREDITOS NAS CADEIRAS"
			$Texto.text = textCredits
		elif nome == "tcc":
			$Titulo.text = "NÃO PODE ESTÁGIO, NÃO PODE TCC"
			$Texto.text = textTcc

func _mouse_dentro(nome):
	if nome == "Voltar":
		$tween.interpolate_property($Voltar, 'scale', $Voltar.scale, Vector2(1.05,1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$tween.start()
	elif nome == "creditos":
		$tween.interpolate_property($IconeCreditosDasCadeiras, 'scale', $IconeCreditosDasCadeiras.scale, Vector2(1.05, 1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "fragil":
		$tween.interpolate_property($iconeFragilidadeEconomicaRU, 'scale', $iconeFragilidadeEconomicaRU.scale, Vector2(1.05, 1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "tcc":
		$tween.interpolate_property($IconeNaoPodeTCCnemTrabalho, 'scale', $IconeNaoPodeTCCnemTrabalho.scale, Vector2(1.05, 1.05), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	
func _mouse_fora(nome):
	if nome == "Voltar":
		$tween.interpolate_property($Voltar, 'scale', $Voltar.scale, Vector2(1,1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$tween.start()
	elif nome == "creditos":
		$tween.interpolate_property($IconeCreditosDasCadeiras, 'scale', $IconeCreditosDasCadeiras.scale, Vector2(1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "fragil":
		$tween.interpolate_property($iconeFragilidadeEconomicaRU, 'scale', $iconeFragilidadeEconomicaRU.scale, Vector2(1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
	elif nome == "tcc":
		$tween.interpolate_property($IconeNaoPodeTCCnemTrabalho, 'scale', $IconeNaoPodeTCCnemTrabalho.scale, Vector2(1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$tween.start()
		
func _clicado(nome):
	if nome == "Voltar":
		get_tree().change_scene("res://Scenes/Tela Vitrine.tscn")